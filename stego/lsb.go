package stego

import (
	"encoding/binary"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"io"
	"os"
)

const (
	headersize int = 20
)

type lsb struct {
}

func getbit(b byte, n int) bool {
	return (b & (1 << n)) != 0
}

func setLsb(b *byte, bit bool) {
	if bit {
		*b = *b | 1
	} else {
		*b = *b & 0xFE // 11111110
	}
}

func setBit(b *byte, bit bool, n int) {
	if bit {
		*b = *b | (1 << n)
	} else {
		*b = *b & ^(1 << n)
	}
}

var errEndOfCarrier = fmt.Errorf("End Of Carrier File")

// hideByte takes a starting location and a byte and places it in the image
func hideByte(b byte, img *image.RGBA, x, y *int) (err error) {
	for i := 0; i < 8; {
		color := img.RGBAAt(*x, *y) // get current pixel color

		// red
		bit := getbit(b, i)
		setLsb(&color.R, bit)
		i++

		if i < 8 {
			// blue
			bit = getbit(b, i)
			setLsb(&color.B, bit)
			i++
		}

		if i < 8 {
			// green
			bit = getbit(b, i)
			setLsb(&color.G, bit)
			i++
		}

		img.SetRGBA(*x, *y, color)
		*y++
		if *y >= img.Bounds().Dy() { // handle img bounds
			*y = 0
			*x++
			if *x >= img.Bounds().Dy() {
				err = errEndOfCarrier
				return
			}
		}
	}
	return
}

func showByte(b *byte, img *image.RGBA, x, y *int) (err error) {
	for i := 0; i < 8; {
		color := img.RGBAAt(*x, *y) // get current pixel color

		// red
		bit := getbit(color.R, 0)
		setBit(b, bit, i)
		i++

		if i < 8 {
			// blue
			bit = getbit(color.B, 0)
			setBit(b, bit, i)
			i++
		}

		if i < 8 {
			// green
			bit = getbit(color.G, 0)
			setBit(b, bit, i)
			i++
		}

		*y++
		if *y >= img.Bounds().Dy() { // handle img bounds
			*y = 0
			*x++
			if *x >= img.Bounds().Dy() {
				err = errEndOfCarrier
				return
			}
		}
	}
	return
}

func writeHeader(img *image.RGBA, bits uint32) {
	bs := make([]byte, 4)
	binary.LittleEndian.PutUint32(bs, bits)
	x, y := 0, 0
	for _, b := range bs {
		_ = hideByte(b, img, &x, &y)
	}
}

func readHeader(img *image.RGBA) (size uint32) {
	bs := make([]byte, 4)

	x, y := 0, 0
	for i, _ := range bs {
		err := showByte(&bs[i], img, &x, &y)
		if err != nil {
			fmt.Println(err)
		}
	}

	size = binary.LittleEndian.Uint32(bs)
	return
}

func OpenAsRGBA(path string) (img *image.RGBA, err error) {
	carrier, _ := os.Open(path)
	carrierImg, _, err := image.Decode(carrier)
	if err != nil {
		return
	}
	// build new RGBA image to manipulate
	img = image.NewRGBA(image.Rect(0, 0, carrierImg.Bounds().Dx(), carrierImg.Bounds().Dy()))
	draw.Draw(img, img.Bounds(), carrierImg, carrierImg.Bounds().Min, draw.Src)

	return
}

func (l *lsb) Hide(payloadPath, carrierPath, resultPath string) (err error) {
	var carrierRGB *image.RGBA
	carrierRGB, err = OpenAsRGBA(carrierPath)

	payload := make([]byte, 1024)
	payloadF, _ := os.Open(payloadPath)
	pN, _ := payloadF.Read(payload) // pN == current bytes of payload

	pBytes := uint32(0)
	i := 0
	x := 0
	y := headersize // leave room for header

	for pN != 0 {
		for pN != 0 {
			_ = hideByte(payload[i], carrierRGB, &x, &y)
			i++
			pBytes++
			pN--
		}
		pN, err = payloadF.Read(payload) // pN == current bytes of payload
		if err != nil {
			if err == io.EOF {
				break
			}
			fmt.Println(err)
			return
		}
	}
	writeHeader(carrierRGB, pBytes)

	var result *os.File
	if result, err = os.OpenFile(resultPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644); err != nil {
		return
	}
	png.Encode(result, carrierRGB)

	return
}

func (l *lsb) Show(suspectPath string) (err error) {
	var suspect *image.RGBA
	if suspect, err = OpenAsRGBA(suspectPath); err != nil {
		fmt.Println("Could Not open Carrier", err)
		return
	}

	messageLength := int(readHeader(suspect))
	fmt.Println("Secret Message length = ", messageLength)
	payload := make([]byte, messageLength)
	x := 0
	y := headersize // leave room for header
	for i := 0; i < messageLength; i++ {
		if err = showByte(&payload[i], suspect, &x, &y); err != nil {
			fmt.Println("Could Not Show Byte", i, err)
			return
		}
	}
	fmt.Println("decoded message = ", string(payload))

	return
}
