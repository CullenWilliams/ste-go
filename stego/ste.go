package stego

import (
	"fmt"
	"path/filepath"
	// something memory related
)

type steg interface {
	Hide(payloadPath, carrierPath, resultPath string) error
	Show(suspectPath string) error
}

func Hide(payloadPath, carrierPath, resultPath string) {
	if payloadPath == "" {
		fmt.Println("usage: stego hide /path/to/payload")
		return
	}
	//FUTURE if payloadPath is not filepath, payload = payloadPath
	payloadPath = filepath.Clean(payloadPath)
	carrierPath = filepath.Clean(carrierPath)
	resultPath = filepath.Clean(resultPath)
	fmt.Println("hiding", payloadPath, "in", carrierPath, "as", resultPath)

	//FUTURE switch methods here
	// create method obj
	l := lsb{}
	// call method.Hide(payload,*File carrier,resultPath)
	l.Hide(payloadPath, carrierPath, resultPath)

}

func Show(suspectPath string) {
	if suspectPath == "" {
		fmt.Println("usage: stego show /path/to/payload")
		return
	}
	suspectPath = filepath.Clean(suspectPath)
	l := lsb{}
	l.Show(suspectPath)
}

// for future
func Detect(suspectPath, originalPath string) {
	if originalPath == "" {
		// google for originals
	}
	// print original size, suspect size
	// for methods
	// try method.Show()
	// if success return result

}
