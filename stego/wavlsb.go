package stego

import (
	"fmt"
	"math/rand"
	"time"

	"os"

	"github.com/cryptix/wav"
)

const ()

var ()

type Wavlsb struct {
	payloadIndex int
	payloadBit   int
	payload      []byte
}

/*
func writeHeader(size uint32) {

}

func readHeader() (size uint32) {

	return
}
*/
func getbitInt(b int32, n int) bool {
	return (b & (1 << n)) != 0
}

func setLsbInt(num *int32, bit bool) {
	if bit {
		*num = *num | 1
	} else {
		*num = *num & ^(1)
	}
}

func (l *Wavlsb) getPayloadBit() bool {
	return (l.payload[l.payloadIndex] & (1 << l.payloadBit)) != 0
}

func (l *Wavlsb) putPayloadBit(b bool) {
	setBit(&l.payload[l.payloadIndex], b, l.payloadBit)
}

func (l *Wavlsb) incrementPayloadIndex() {
	if l.payloadIndex >= len(l.payload) {
		return // do nothing
	}
	l.payloadBit++
	if l.payloadBit >= 8 {
		l.payloadBit = 0
		l.payloadIndex++
	}
}

func (l *Wavlsb) hideBit(in *int32, b bool) {
	setLsbInt(in, b)
	return
}

// HideInNoise conceals the payload within a white noise audio file
func (l *Wavlsb) HideInNoise(payloadPath, resultPath string) (err error) {
	l.payloadIndex = 0
	l.payloadBit = 0
	l.payload = make([]byte, 1024)
	payloadF, _ := os.Open(payloadPath)
	n, _ := payloadF.Read(l.payload) // pN == current bytes of payload
	l.payload = l.payload[:n]

	result, err := os.Create(resultPath + ".wav")
	if err != nil {
		return
	}
	defer result.Close()

	meta := wav.File{
		Channels:        1,
		SampleRate:      44100,
		SignificantBits: 32,
	}

	writer, err := meta.NewWriter(result)
	if err != nil {
		return
	}
	defer writer.Close()

	// write header lazy way (it's just the first int32)
	fmt.Println("Message length =", len(l.payload))
	err = writer.WriteInt32(int32(len(l.payload)))
	if err != nil {
		fmt.Println(err)
		return
	}

	duration := 30 // seconds
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < duration*44100; i++ {
		y := rand.Int31() // generate random value

		if l.payloadIndex < len(l.payload) {
			b := l.getPayloadBit()
			l.incrementPayloadIndex()
			l.hideBit(&y, b)
		}

		err = writer.WriteInt32(y)
		if err != nil {
			return
		}
	}

	return
}

func (l *Wavlsb) Hide(payloadPath, carrierPath, resultPath string) (err error) {
	l.payloadIndex = 0
	l.payloadBit = 0
	l.payload = make([]byte, 1024)
	payloadF, _ := os.Open(payloadPath)
	n, _ := payloadF.Read(l.payload) // pN == current bytes of payload
	l.payload = l.payload[:n]

	// open carrier
	info, err := os.Stat(carrierPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	cWav, err := os.Open(carrierPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	wavReader, err := wav.NewReader(cWav, info.Size())
	if err != nil {
		fmt.Println("could not create reader for", carrierPath, err)
		return
	}
	fmt.Println(wavReader)
	// get wavReader metadata

	result, err := os.Create(resultPath + ".wav")
	if err != nil {
		fmt.Println(err)
		return
	}

	// create output writer
	rmeta := wavReader.GetFile()
	meta := wav.File{
		Channels:        rmeta.Channels,
		SampleRate:      rmeta.SampleRate,
		SignificantBits: rmeta.SignificantBits,
		NumberOfSamples: rmeta.NumberOfSamples,
		Duration:        rmeta.Duration,
	}
	writer, err := meta.NewWriter(result)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer writer.Close()
	// read payload

	// write header lazy way (it's just the first int32)
	fmt.Println("Message length =", len(l.payload))
	_, _ = wavReader.ReadSample() // throw away
	err = writer.WriteInt32(int32(len(l.payload)))
	if err != nil {
		fmt.Println(err)
		return
	}

	// for b in payload write to carrier
	for i := uint32(0); i < uint32(meta.Duration.Seconds())*meta.SampleRate; i++ {
		y, _ := wavReader.ReadSample()

		if l.payloadIndex < len(l.payload) {
			b := l.getPayloadBit()
			l.incrementPayloadIndex()
			l.hideBit(&y, b)
		}

		err = writer.WriteInt32(y)
		if err != nil {
			return
		}
	}

	return
}

func (l *Wavlsb) Show(suspectPath string) (err error) {
	// open suspect
	info, err := os.Stat(suspectPath)
	if err != nil {
		return
	}
	cWav, err := os.Open(suspectPath)
	if err != nil {
		return
	}
	wavReader, err := wav.NewReader(cWav, info.Size())
	fmt.Println(wavReader)

	// read secret size
	size, err := wavReader.ReadSample()
	fmt.Println("Secret message length = ", size)

	l.payloadIndex = 0
	l.payloadBit = 0
	l.payload = make([]byte, size)

	for i := int32(0); i < size*8; i++ {
		y, _ := wavReader.ReadSample()

		b := getbitInt(y, 0)
		l.putPayloadBit(b)
		l.incrementPayloadIndex()

	}

	fmt.Println("decoded message = ", string(l.payload))

	return
}
