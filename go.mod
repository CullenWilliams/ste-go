module gitlab.com/CullenWilliams/ste-go

go 1.15

require (
	github.com/cryptix/wav v0.0.0-20180415113528-8bdace674401
	github.com/smartystreets/goconvey v1.6.4
)
