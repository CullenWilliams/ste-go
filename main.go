package main

import (
	//"flag"
	//"os"

	"gitlab.com/CullenWilliams/ste-go/stego"
)

func main() {
	s := stego.Wavlsb{}
	//s.Hide("resources/secretmessage.txt", "resources/lands_of_ice_mono.wav", "output")
	s.HideInNoise("resources/secretmessage.txt", "output")
	s.Show("output.wav")
	/*
		stego.Hide("resources/secretmessage.txt", "resources/p2.png", "output.png")
		stego.Show("output.png")
	*/

	/*
		// ------------ flags ----------------
		carrierPath := flag.String("in", "", "file to conceal message in. e.g. /path/to/carrier.xxx")
		resultPath := flag.String("as", "", "output file name. defaults to /path/to/carrier_result.xxx")
		// -s for string payload

		// add flags for each steganographic method as they are developed
		// ----------------------------------
		flag.Parse()

		_ = carrierPath
		_ = resultPath

		switch os.Args[1] {

		case "show":
			// called as "stego show filename"
			// tries to extract hidden message with all/given methods
			stego.Show(os.Args[2])
		case "hide":
			// called as stego hide filename.txt (-in carrier.jpg)
			// hides given message in a specified/default carrier file
			//stego.Hide(os.Args[2], *carrierPath, *resultPath)
			stego.Hide(os.Args[2], "resources/p2.png", "output.png")

		}
	*/
}
